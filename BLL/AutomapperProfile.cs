﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<History, HistoryDTO>()
               .ForMember(p => p.TestId, c => c.MapFrom(history => history.Test.Id))
               .ReverseMap();

            CreateMap<TestType, TestTypeDTO>().ReverseMap();

            CreateMap<Question, QuestionToPassDTO>().ReverseMap();

            CreateMap<Answer, AnswerDTO>()
                .ForMember(p => p.IsRightAnswer, c => c.Ignore())
                .ReverseMap();

            CreateMap<Test, TestDTO>()
               .ForMember(p => p.QuestionsIds, c => c.MapFrom(question => question.Questions.Select(x => x.Id)))
               .ForMember(p => p.UsersIds, c => c.MapFrom(test => test.Tests.Select(x => x.UserId)))
               .ReverseMap();

            CreateMap<Test, TestToPassDTO>()
                .ForMember(p => p.Questions, c => c.MapFrom(question => question.Questions))
                .ForMember(p => p.UsersIds, c => c.MapFrom(test => test.Tests.Select(x => x.UserId)))
                .ReverseMap();
        }
    }
}
