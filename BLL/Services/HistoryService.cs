﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class HistoryService: IHistoryService
    {
        IUnitOfWork uow { get; set; }
        IMapper map { get; set; }

        public HistoryService(IUnitOfWork _uow, IMapper _map)
        {
            uow  = _uow;
            map = _map;
        }

        public IEnumerable<HistoryDTO> GetAll()
        {
            var result =
                from history in uow.HistoryRepository.GetAll()
                select map.Map<HistoryDTO>(history);
            return result;
        }


        public async Task<HistoryDTO> GetByIdAsync(int id)
        {
            var result = await uow.HistoryRepository.GetByIdAsync(id);
            if (result!=null)
                throw new LabException();
            return map.Map<HistoryDTO>(result);
        }

        public async Task AddAsync(HistoryDTO history)
        {
            if (!HistoryIsValid.IsValid(history))
                throw new LabException("History is not valid!");
            History newHistory = map.Map<History>(history);
            await uow.HistoryRepository.AddAsync(newHistory);
            await uow.SaveAsync();
            history.Id = newHistory.Id;
        }

        public async Task UpdateAsync(HistoryDTO history)
        {
            if (!HistoryIsValid.IsValid(history))
                throw new LabException("History is not valid!");
            History newHistory = map.Map<History>(history);
            uow.HistoryRepository.Update(newHistory);
            await uow.SaveAsync();
            history.Id = newHistory.Id;
        }

        public async Task DeleteByIdAsync(int historyId)
        {
            await uow.HistoryRepository.DeleteByIdAsync(historyId);
            await uow.SaveAsync();
        }


        public async Task MakeHistoryTestPass(TestToPassDTO test, string userid)
        {
            var testCheck = await uow.TestRepository.GetByIdAsync(test.Id);
            var rightAnswers =
                from question in testCheck.Questions
                from answer in question.Answers
                select answer;

            var calculateRightAnswers =
                from question in test.Questions
                from answer in question.Answers
                from rightAns in rightAnswers
                where (answer.Id == rightAns.Id && answer.IsRightAnswer==rightAns.IsRightAnswer && rightAns.IsRightAnswer)
                select rightAns;

            var calculateWrongAnswers =
                from question in test.Questions
                from answer in question.Answers
                from rightAns in rightAnswers
                where (answer.Id == rightAns.Id && !rightAns.IsRightAnswer && answer.IsRightAnswer)
                select rightAns;
            var score=calculateRightAnswers.Count() - calculateWrongAnswers.Count();

            if (score < 0) 
                score = 0;

            HistoryDTO history = new HistoryDTO
            {
                UserId = userid,
                TestId=test.Id,
                DatePass=DateTime.Now,
                Score=score*10
            };
            if (!HistoryIsValid.IsValid(history))
                throw new LabException("History is not valid!");
            History newHistory = map.Map<History>(history);
            await uow.HistoryRepository.AddAsync(newHistory);
            history.Id = newHistory.Id;
            await uow.SaveAsync();
        }
    }
}
