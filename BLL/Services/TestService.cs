﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TestService: ITestService
    {
        IUnitOfWork uow { get; set; }
        IMapper map { get; set; }

        public TestService(IUnitOfWork _uow, IMapper _map)
        {
            uow  = _uow;
            map = _map;
        }

        public IEnumerable<TestDTO> GetAll()
        {
            var result =
                from test in uow.TestRepository.GetAll()
                select map.Map<TestDTO>(test);
            return result;
        }


        public async Task<TestDTO> GetByIdAsync(int id)
        {
            var result = await uow.TestRepository.GetByIdAsync(id);
            if (result==null)
                throw new LabException("This test not found!");
            return map.Map<TestDTO>(result);
        }

        public async Task AddAsync(TestDTO test)
        {
            if (!TestIsValid.IsValid(test))
                throw new LabException("Test is not valid!");
            Test newTest = map.Map<Test>(test);
            await uow.TestRepository.AddAsync(newTest);
            await uow.SaveAsync();
            test.Id = newTest.Id;
        }

        public async Task UpdateAsync(TestDTO test)
        {
            if (!TestIsValid.IsValid(test))
                throw new LabException("Test is not valid!");
            Test newTest = map.Map<Test>(test);
            uow.TestRepository.Update(newTest);
            await uow.SaveAsync();
            test.Id = newTest.Id;
        }

        public async Task DeleteByIdAsync(int testId)
        {
            await uow.TestRepository.DeleteByIdAsync(testId);
            await uow.SaveAsync();
        }


        public async Task<TestToPassDTO> GetModelTestById(int id)
        {
            var result = await uow.TestRepository.GetByIdAsync(id);
            if (result == null)
                throw new LabException("This test not found!");
            return map.Map<TestToPassDTO>(result);
        }
    }
}
