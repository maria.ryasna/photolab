﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class TestDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TestTypeId { get; set; }
        public ICollection<int> QuestionsIds { get; set; }
        public ICollection<string> UsersIds { get; set; }
    }
}
