﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class AnswerDTO
    {
        public int Id { get; set; }
        public int QuestionId { get; set; }
        public string Name { get; set; }
        public bool IsRightAnswer { get; set; }
    }
}
