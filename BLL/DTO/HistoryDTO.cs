﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class HistoryDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int TestId { get; set; }
        public DateTime DatePass { get; set; }
        public int Score { get; set; }
    }
}
