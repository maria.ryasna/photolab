﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class QuestionToPassDTO
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public string Statement { get; set; }
        public ICollection<AnswerDTO> Answers { get; set; }
    }
}
