﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class TestToPassDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TestTypeDTO TestType { get; set; }
        public ICollection<QuestionToPassDTO> Questions { get; set; }
        public ICollection<string> UsersIds { get; set; }
    }
}
