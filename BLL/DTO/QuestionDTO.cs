﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class QuestionDTO
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public string Statement { get; set; }
        public ICollection<int> AnswersIds { get; set; }
    }
}
