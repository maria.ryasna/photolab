﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IHistoryService: ICrud<HistoryDTO>
    {
        public Task MakeHistoryTestPass(TestToPassDTO test, string userid);
    }
}
