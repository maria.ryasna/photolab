﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITestService: ICrud<TestDTO>
    {
        Task<TestToPassDTO> GetModelTestById(int id);
    }
}
