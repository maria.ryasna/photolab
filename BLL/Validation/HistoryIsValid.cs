﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation
{
    public static class HistoryIsValid
    {
        public static bool IsValid(HistoryDTO history)
        {
            if (history.DatePass == DateTime.MinValue)
                return false;
            if (history.UserId == "")
                return false;
            if (history.TestId == int.MinValue)
                return false;
            return true;
        }
    }
}
