﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation
{
    public static class TestIsValid
    {
        public static bool IsValid(TestDTO test)
        {
            if (test.Name=="" || test.TestTypeId==0)
                return false;
            return true;
        }
    }
}
