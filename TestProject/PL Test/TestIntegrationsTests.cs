﻿using Library.Tests.IntegrationTests;
using Newtonsoft.Json;
using NUnit.Framework;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.PL_Test
{
    [TestFixture]
    public class TestIntegrationsTests
    {
        
        private HttpClient _client;
        private WebApplicationFactory _factory;
        private const string RequestUri = "api/test/";

        [SetUp]
        public void SetUp()
        {
            _factory = new WebApplicationFactory();
            _client = _factory.CreateClient();
        }

        [Test]
        public async Task TestsController_GetByFilter_ReturnsAllWithNullFilter()
        {
            var httpResponse = await _client.GetAsync(RequestUri);

            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var tests = JsonConvert.DeserializeObject<IEnumerable<TestModel>>(stringResponse);

            Assert.AreEqual(2, tests.Count());
        }
    }
}
