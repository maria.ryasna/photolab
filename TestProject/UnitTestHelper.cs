using AutoMapper;
using BLL;
using DAL;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;

namespace TestProject
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<QuizDbContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<QuizDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new QuizDbContext(options))
            {
                SeedData(context);
            }
            return options;
        }

        public static void SeedData(QuizDbContext context)
        {
            context.TestTypes.Add(new TestType { Id = 1, Name = "Beginner" });
            context.TestTypes.Add(new TestType { Id = 2, Name = "Middle" });
            context.Tests.Add(new Test { Id = 1, TestTypeId = 1, Name = "First test" });
            context.Tests.Add(new Test { Id = 2, TestTypeId = 2, Name = "Second test" });
            context.Questions.Add(new Question { Id = 1, TestId=1, Statement="First question"});
            context.Questions.Add(new Question { Id = 2, TestId = 2, Statement = "Second question" });
            context.Answers.Add(new Answer { Id = 1, QuestionId=1, Name="First answer", IsRightAnswer=true});
            context.Answers.Add(new Answer { Id = 2, QuestionId = 1, Name = "Second answer", IsRightAnswer = false });
            context.SaveChanges();
        }

        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}