﻿using DAL;
using DAL.Entities;
using DAL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.DAL_Test
{
    [TestFixture]
    public class TestRepositoryTests
    {
        [Test]
        public void TestRepository_GetAllTests()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testRepository = new TestRepository(context);

                var types = testRepository.GetAll();

                Assert.AreEqual(2, types.Count());
            }
        }

        [Test]
        public async Task TestRepository_GetById()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testTypeRepository = new TestTypeRepository(context);

                var testType = await testTypeRepository.GetByIdAsync(1);

                Assert.AreEqual(1, testType.Id);
                Assert.AreEqual("Beginner", testType.Name);
            }
        }

        [Test]
        public async Task TestRepository_AddAsync_AddsValueToDatabase()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testsRepository = new TestRepository(context);
                var test = new Test() { Id = 3 };

                await testsRepository.AddAsync(test);
                await context.SaveChangesAsync();

                Assert.AreEqual(3, context.Tests.Count());
            }
        }

        [Test]
        public async Task TestRepository_DeleteByIdAsync_DeletesEntity()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testRepository = new TestRepository(context);

                await testRepository.DeleteByIdAsync(1);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, context.Tests.Count());
            }
        }

        [Test]
        public async Task TestRepository_Update_UpdatesEntity()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testsRepository = new TestRepository(context);

                var test = new Test() { Id = 1, Name = "Photo test", TestTypeId=1 };

                testsRepository.Update(test);
                await context.SaveChangesAsync();

                Assert.AreEqual(1, test.Id);
                Assert.AreEqual("Photo test", test.Name);
                Assert.AreEqual(1, test.TestTypeId);
            }
        }


        [Test]
        public async Task TestsRepository_GetByIdAsync_ReturnsWithIncludedTestTypeEntity()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var expectedTestTypeId = 2;
                var testsRepository = new TestRepository(context);
                var testWithIncludes = await testsRepository.GetByIdAsync(2);

                var actual = testWithIncludes.TestTypeId;

                Assert.AreEqual(expectedTestTypeId, actual);
            }
        }
    }
}
