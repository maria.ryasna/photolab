﻿using DAL;
using DAL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.DAL_Test
{
    [TestFixture]
    public class TestTypeRepositoryTests
    {
        [Test]
        public void TestTypeRepository_ReturnAllTypes()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testTypeRepository = new TestTypeRepository(context);

                var types = testTypeRepository.GetAll();

                Assert.AreEqual(2, types.Count());
            }
        }

        [Test]
        public async Task TestTypeRepository_GetById()
        {
            using (var context = new QuizDbContext(UnitTestHelper.GetUnitTestDbOptions()))
            {
                var testRepository = new TestRepository(context);

                var test = await testRepository.GetByIdAsync(1);

                Assert.AreEqual(1, test.Id);
                Assert.AreEqual("First test", test.Name);
                Assert.AreEqual(1, test.TestType.Id);
            }
        }
    }
}