﻿using BLL.DTO;
using BLL.Interfaces;
using BLL.Services;
using BLL.Validation;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using NUnit.Framework;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.BLL_Test
{
    public class TestServiceTests
    {
        [Test]
        public void TestService_GetAll_ReturnsTestModels()
        {
            // Arrange
            var expected = GetTestTestsModels().ToList();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.TestRepository.GetAll())
                .Returns(GetTestTestsEntities().AsQueryable);
            ITestService testService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //Act
            var actual = testService.GetAll().ToList();

            //Assert
            for (int i = 0; i < actual.Count; i++)
            {
                Assert.AreEqual(expected[i].Id, actual[i].Id);
                Assert.AreEqual(expected[i].Name, actual[i].Name);
                Assert.AreEqual(expected[i].TestTypeId, actual[i].TestTypeId);
            }
        }


        [Test]
        public async Task TestService_GetByIdAsync_ReturnsTestModel()
        {
            // Arrange
            var expected = GetTestTestsModels().First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(m => m.TestRepository.GetByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(GetTestTestsEntities().First);
            ITestService bookService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //Act
            var actual = await bookService.GetByIdAsync(1);

            //Assert
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.TestTypeId, actual.TestTypeId);
        }

        [Test]
        public async Task TestService_AddAsync_AddsModel()
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.AddAsync(It.IsAny<Test>()));
            ITestService testService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var test = new TestDTO { Id = 100, Name = "Add new test", TestTypeId=1};

            //Act
            await testService.AddAsync(test);

            //Assert
            mockUnitOfWork.Verify(x => x.TestRepository.AddAsync(It.Is<Test>(b => b.Name == test.Name && b.TestTypeId == test.TestTypeId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void TestService_AddAsync_ThrowsLabExceptionWithEmptyName()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.AddAsync(It.IsAny<Test>()));
            ITestService bookService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var test = new TestDTO { Id = 1, Name = "", TestTypeId = 1 };

            Assert.ThrowsAsync<LabException>(() => bookService.AddAsync(test));
        }


        [Test]
        public void TestService_AddAsync_ThrowsLabExceptionWithEmptyTestType()
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.AddAsync(It.IsAny<Test>()));
            ITestService bookService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var test = new TestDTO { Id = 1, Name = "Test add exception" };

            //Assert
            Assert.ThrowsAsync<LabException>(() => bookService.AddAsync(test));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(100)]
        public async Task TestService_DeleteByIdAsync_DeletesTest(int testId)
        {
            //Arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.DeleteByIdAsync(It.IsAny<int>()));
            ITestService testService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //Act
            await testService.DeleteByIdAsync(testId);

            //Assert
            mockUnitOfWork.Verify(x => x.TestRepository.DeleteByIdAsync(testId), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public async Task TestService_UpdateAsync_UpdatesTest()
        {
            //Arrange
            var test = new TestDTO { Id = 1, Name = "Test some", TestTypeId=1 };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.Update(It.IsAny<Test>()));
            ITestService testService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //Act
            await testService.UpdateAsync(test);

            //Assert
            mockUnitOfWork.Verify(x => x.TestRepository.Update(It.Is<Test>(b => b.Name == test.Name && b.TestTypeId == test.TestTypeId)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void TestService_UpdateAsync_ThrowsLabExceptionWithEmptyName()
        {
            //Arrange
            var test = new TestDTO { Id = 1, Name = "", TestTypeId=1 };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.TestRepository.Update(It.IsAny<Test>()));
            ITestService testService = new TestService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //Assert
            Assert.ThrowsAsync<LabException>(() => testService.UpdateAsync(test));
        }

        private IEnumerable<TestDTO> GetTestTestsModels()
        {
            return new List<TestDTO>()
            {
                new TestDTO {Id = 1, Name="test 1", TestTypeId=1},
                new TestDTO {Id = 1, Name="test 2", TestTypeId=2}
            };
        }


        private List<Test> GetTestTestsEntities()
        {
            return new List<Test>()
            {
                new Test {Id = 1, Name="test 1", TestTypeId=1},
                new Test {Id = 1, Name="test 2", TestTypeId=2}
            };
        }

    }
}
