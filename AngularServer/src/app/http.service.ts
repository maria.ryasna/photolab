import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Test } from './Models/test';
import { TestToPass } from './Models/testtopass';
import { TestSend } from './Models/testsend';
import { Router } from '@angular/router';

   
@Injectable()
export class HttpService {
   
    constructor(private http: HttpClient, private router: Router){ }
      
    getStatistic(): Observable<History[]>{
        return this.http.get<History[]>('https://localhost:44355/api/statistic');
    }

    getTestById(id: number): Observable<Test>{
        return this.http.get<Test>('https://localhost:44355/api/test/'+id);
    }

    getTestToPass(id: number):Observable<TestToPass>{
        return this.http.get<TestToPass>('https://localhost:44355/api/test/pass/'+id);
    }

    getTest(): Observable<Test[]>{
        return this.http.get<Test[]>('https://localhost:44355/api/test');
    }

    sendTestPass(testAnswer: TestSend) {
          
        this.http.post('https://localhost:44355/api/statistic/receivetest', testAnswer).subscribe(
            (resp:any)=> 
            {this.router.navigate(['layout/statistic']);    }
        );
            
    }

    deleteHistory(id: number) {
        return this.http.delete('https://localhost:44355/api/statistic/delete/'+id);
    }

}
