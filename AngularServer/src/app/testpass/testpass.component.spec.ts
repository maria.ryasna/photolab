import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestpassComponent } from './testpass.component';

describe('TestpassComponent', () => {
  let component: TestpassComponent;
  let fixture: ComponentFixture<TestpassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestpassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
