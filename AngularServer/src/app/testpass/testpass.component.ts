import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { HttpService } from '../http.service';
import { Test } from '../Models/test';
import { TestSend } from '../Models/testsend';
import { TestToPass } from '../Models/testtopass';

@Component({
  selector: 'app-testpass',
  templateUrl: './testpass.component.html',
  styleUrls: ['./testpass.component.css']
})
export class TestpassComponent implements OnInit {
    id: number|any;
    test: Test|any;
    testToPass: TestToPass|any;
    testAnswer: TestSend|any;
    userName: string|any;

public onSaveUserAnswer(value:boolean, questId: number, answId: number){
    (this.testAnswer.test
      .questions.find((x: { id: number; })=>x.id==questId))
      .answers.find((x: { id: number; })=>x.id==answId).isRightAnswer=value;
}
    constructor(private activateRoute: ActivatedRoute, private httpService: HttpService, private authService: AuthService){    
      
    }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];        
      this.userName=this.authService.getUserName();
      this.httpService.getTestToPass(this.id).subscribe(data => {
        this.testToPass = data;
        this.testAnswer=new TestSend(this.userName, data);
      });
  }

  TestPass() {
    this.httpService.sendTestPass(this.testAnswer);
  }

  
}
