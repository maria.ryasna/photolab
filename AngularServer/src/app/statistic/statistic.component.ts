import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css'],
  providers: [HttpService]
})
export class StatisticComponent implements OnInit {
  errorMessage: any;

  constructor(private httpService: HttpService, private authService: AuthService) { }
  done: boolean = true;
  public recHistory: History[]|any;
  ngOnInit(): void {
    this.httpService.getStatistic()
    .subscribe(
        (data) => {this.recHistory=data; this.done=true;},
        error => console.log(error)
    );
  }
  isAdmin() {
    if (this.authService.GetRole()=="Admin")
      return true;
    return false;
  }

  deleteHistory(id: number) {
    this.httpService.deleteHistory(id)
    .subscribe({
      next: (data:any) => {
          console.log('Delete successful');
      },
      error: (error:any) => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
     }
  });
  location.reload();
  }
  
  editUser(id: number) {

  }

}
