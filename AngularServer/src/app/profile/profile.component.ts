import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user:string| any;
  constructor(private authService: AuthService) {
    
   }

  ngOnInit(): void {
  }

  isAuth():boolean
  {
    this.user=localStorage.getItem('username');
    if (this.authService.isAuthenticated())
      return false;
    else
      return true;
  }

}
