import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../Models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myForm : FormGroup = new FormGroup({
    "userName": new FormControl("", [
      Validators.required, 
      Validators.pattern("[a-zA-Z ]*"),
      Validators.minLength(3)
]),
             
    "userEmail": new FormControl("", [
                Validators.required, 
                Validators.email,
                Validators.minLength(3)
    ]),
    "userPassword": new FormControl("", 
    [Validators.required, 
    Validators.minLength(3)]) 
});

  user: User|any;
  error:any;
  rememberMe: any=false;
  constructor(private authService: AuthService, private router: Router) {
  }
  Register() {
  if (this.myForm.valid)
  {
    this.user=new User(this.myForm.value.userEmail, this.myForm.value.userName, this.myForm.value.userPassword, true);
    this.authService.postRegister(this.user)
    .subscribe((resp: any) => {
      this.router.navigate(['layout/login']);
      },
      (error) => {                              
        this.error = error.error;
      }); 
    }
    else
      this.error="The request could not be sent. Please, enter correct information!";
  }
 
  ngOnInit() { }
}

