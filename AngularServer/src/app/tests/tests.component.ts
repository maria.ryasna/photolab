import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Test } from '../Models/test';
import { TestType } from '../Models/testtype';
import { map, delay, mergeAll, concatMap, switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.css'],
  providers: [HttpService]
})
export class TestsComponent implements OnInit {
  constructor(private httpService: HttpService) { }
  public recTest: Test[]|any;
  public filterTest: Test[]|any;
  public testType: TestType|any;
  public filter: any;
  ngOnInit(): void {

    this.httpService.getTest().subscribe(data => {
        this.recTest = data;
        this.filterTest=data;
      });
  }

  FilterTest()
  {
    //this.recTest=this.filterTest;
    //this.recTest=this.filterTest.some((s:any) =>
        //s.name.includes(this.filter));
  //  this.recTest=this.filterTest.some((s:any) =>
        //s.name.includes(this.filter));

        this.recTest=this.filterTest.filter((p:any) => (p.name.toLowerCase())
         .includes((this.filter).toLowerCase()));
    }

  FilterClear() 
  {
    this.recTest=this.filterTest;
    this.filter="";
  }


}
