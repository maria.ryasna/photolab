import { Component, OnInit } from '@angular/core';
import { tokenGetter } from '../app.module';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  tokenGetter: boolean=false;
  constructor(private authService: AuthService) {
   }

  ngOnInit(): void {

  }

  isAuth():boolean
  {
    if (this.authService.isAuthenticated())
      return false;
    else
      return true;
  }

}
