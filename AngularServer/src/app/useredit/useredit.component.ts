import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';
import { User } from '../Models/user';

@Component({
  selector: 'app-useredit',
  templateUrl: './useredit.component.html',
  styleUrls: ['./useredit.component.css']
})
export class UsereditComponent implements OnInit {
  id: any;
  userName: string | any;
  user: User|any;
  userEmail: any;

  constructor(private activateRoute: ActivatedRoute, private authService: AuthService) { }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];        
    this.authService.getUserById(this.id)
      .subscribe((resp:any)=>
        {
          this.user=resp;
          this.userName=resp.username;
          this.userEmail=resp.email;
        }
      );
      

  }

}
