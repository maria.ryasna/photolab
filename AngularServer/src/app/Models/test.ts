import { TestType } from "./testtype";

export class Test {
    id: number;
    name: string;
    testTypeId: number;
    testType: TestType;
    questionsIds: number[];
    usersIds: string[];
    constructor(id: number,  name: string, testTypeId: number, testType: TestType, questionsIds: number[], usersIds: string[]) {
        this.id=id;
        this.name=name;
        this.testTypeId=testTypeId;
        this.testType=testType;
        this.questionsIds=questionsIds;
        this.usersIds=usersIds;
      }
}
