import { Answer } from "./answer";

export class Question {
        id: number;
        testId: number;
        statement: string;
        answers: Answer[];
        constructor(id: number,  testId: number, statement: string, answers: Answer[]) {
            this.id=id;
            this.testId=testId;
            this.statement=statement;
            this.answers=answers;
        }
}

