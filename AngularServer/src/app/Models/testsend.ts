import { TestToPass } from "./testtopass";

export class TestSend {
    userName: string;
    test: TestToPass;
    constructor(userName: string, test: TestToPass) {
        this.userName=userName;
        this.test=test;
      }
}
