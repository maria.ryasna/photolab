export class History {
    id: number;
    testid: number;
    userid: number;
    datepass: Date;
    score: number;
    constructor(id: number, testid: number, userid: number, datepass: Date, score: number) {
        this.id=id;
        this.testid=testid;
        this.userid=userid;
        this.datepass=datepass;
        this.score=score;
      }
}
