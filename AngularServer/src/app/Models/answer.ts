export class Answer {
    id: number;
    questionId: number;
    name: string;
    isRightAnswer: boolean;
    constructor(id: number,  questionId: number, name: string, isRightAnswer: boolean) {
        this.id=id;
        this.questionId=questionId;
        this.name=name;
        this.isRightAnswer=isRightAnswer;
    }
}
