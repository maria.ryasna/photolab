import { Question } from "./question";
import { TestType } from "./testtype";

export class TestToPass {
    id: number;
    name: string;
    testType: TestType;
    questions: Question[];
    usersIds: string[];
    constructor(id: number,  name: string, testType: TestType, questions: Question[], usersIds: string[]) {
        this.id=id;
        this.name=name;
        this.testType=testType;
        this.questions=questions;
        this.usersIds=usersIds;
      }
}


