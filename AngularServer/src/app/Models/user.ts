export class User {
    email: string;
    username: string;
    password: string;
    rememberMe: boolean;
    constructor(email: string, username: string, password: string, rememberMe: boolean) {
        this.email=email;
        this.username=username;
        this.password=password;
        this.rememberMe=rememberMe;
      }
}
