import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { tokenGetter } from './app.module';
import { User } from './Models/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
    static getToken() {
      return localStorage.getItem('accessToken');;
    }
  uri = 'https://localhost:44355/api/account';
  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelperService) { }

  SaveLocal(resp: any)
  {
    localStorage.setItem('accessToken', resp.value.access_token);
    localStorage.setItem('username', resp.value.username);
    localStorage.setItem('role', resp.value.role);
  }
    GetRole()
    {
      var token=tokenGetter();
      if (token!=null)
        return JSON.parse(window.atob(token.split('.')[1]))["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
      return null;   
    }

    postLogin(user: User) {
      const body = {email: user.email, password: user.password, rememberme: user.rememberMe};
      return this.http.post(this.uri, body);         
  }

  postRegister(user: User):Observable<Object>
  {
    const body = {email: user.email, username: user.username, password: user.password, rememberme: true};
    return this.http.post(this.uri+"/register", body);
    
}

    logout() {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('role');
      localStorage.removeItem('username');
      this.router.navigate(['layout/login']); 
    }
    getToken()
    {
      var token=localStorage.getItem('accessToken');
      return token;
    }
   
    public get logIn(): boolean {
      return (localStorage.getItem('token') !== null);
    }

    public isAuthenticated(): boolean {
      var token=localStorage.getItem('accessToken');
      if (token)
        return !this.jwtHelper.isTokenExpired(token);
      return false;
    }

    public getUserName() {
      var username=localStorage.getItem('username');
      return username;
    }

    public getUserById(id: string):Observable<User>
    {
      return this.http.get<User>(this.uri+"/"+id);
    }
}
