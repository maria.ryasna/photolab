import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation/navigation.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TestsComponent } from './tests/tests.component';
import { StatisticComponent } from './statistic/statistic.component';
import { AboutComponent } from './about/about.component';
import { CommonModule } from '@angular/common';  
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuard } from './auth.guard';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';
import { TestpassComponent } from './testpass/testpass.component';
import { AuthComponent } from './auth/auth.component';
import { TestresultComponent } from './testresult/testresult.component';
import { AdminGuard } from './admin.guard';
import { UsereditComponent } from './useredit/useredit.component';
import { LayoutComponent } from './layout/layout.component';
const helper = new JwtHelperService();
 

export function tokenGetter() {
  return localStorage.getItem('accessToken');

}

const itemRoutes: Routes = [
  { path: 'pass', component: TestpassComponent},
];

const appRoutes: Routes =[
  {path: '', redirectTo: '/layout/home', pathMatch: 'full'},
  {path: 'layout',
  component: LayoutComponent,
  children: [
    {path: 'home', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: 'tests', component: TestsComponent},
    {path: 'statistic', component: StatisticComponent, canActivate: [AuthGuard]},
    {path: 'user/:id', component: UsereditComponent, canActivate: [AdminGuard]},
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'logout', component: LogoutComponent},
    {path: 'tests/:id', component: TestpassComponent, canActivate: [AuthGuard]},
  ]
},

  
//  {path: 'tests/:id/pass', component: TestpassComponent, canActivate: [AuthGuard]},
//  {path: 'tests', component: TestsComponent, canActivate: [AdminGuard]},
  {path: '**', component: PageNotFoundComponent},
  
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    NavigationComponent,
    HomeComponent,
    TestsComponent,
    StatisticComponent,
    AboutComponent,
    PageNotFoundComponent,
    ProfileComponent,
    TestpassComponent,
    AuthComponent,
    TestresultComponent,
    UsereditComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:44355"],
        disallowedRoutes: [],
      }}),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

