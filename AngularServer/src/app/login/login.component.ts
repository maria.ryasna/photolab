import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../Models/user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myForm : FormGroup = new FormGroup({
             
    "userEmail": new FormControl("", [
                Validators.required, 
                Validators.email,
                Validators.minLength(3)
    ]),
    "userPassword": new FormControl("", 
    [Validators.required, 
    Validators.minLength(5)]) 
});
  user:User|any;
  error:any;
  rememberMe: any=false;
  constructor(private authService: AuthService, private router: Router) {
  }

  Login() {
    if (this.myForm.valid)
    {
      this.user=new User(this.myForm.value.userEmail, "", this.myForm.value.userPassword, this.rememberMe);
      this.authService.postLogin(this.user)
      .subscribe((resp: any) => {
        if (resp.statusCode === 200) {
          this.authService.SaveLocal(resp); 
          this.router.navigate(['layout/home']);
        }},
        (error) => {                              
          this.error = error.error;
        });   
    }
    else 
      this.error="The request could not be sent. Please, enter correct information!";
    }

  Remember(check: boolean) {
    this.rememberMe=check;
  }
  ngOnInit() { }
}


