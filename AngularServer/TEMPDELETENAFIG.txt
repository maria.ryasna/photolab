import { Component } from '@angular/core';
import { HttpService} from './http.service';

@Component({
  selector: 'app-root',
  template: `
  <p>HELOOOOOOOOOOOOOOOOO</p>
  <p>{{title}}</p>
  <button (click)="submit()">Отправить</button>
  <div *ngIf="done">
  <div *ngFor="let h of recHistory">
  <p>{{h.id}}</p>
  </div>
  <router-outlet></router-outlet>`,
  providers: [HttpService]
})
export class AppComponent {
  title = 'AngularServer';
  done: boolean = false;
  public recHistory: History[] | undefined;
  
  constructor(private httpService: HttpService){
    this.recHistory={} as History[];
  }
  submit() {
      this.httpService.getTest()
              .subscribe(
                  (data: any) => {this.recHistory=data; this.done=true;},
                  error => console.log(error)
              );
  }

}
