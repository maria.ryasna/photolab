﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace AuthOpt
{
    public class AuthOptions
    {
        // token publisher
        public const string ISSUER = "WebApplicationwWebAPI_JWT_demoServer";
        // token user
        public const string AUDIENCE = "WebApplicationwWebAPI_JWT_demoClient";
        const string KEY = "this is my custom Secret key for authnetication"; // key for encrypting
        public const int LIFETIME = 10; // token ttl - 1 minute
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
