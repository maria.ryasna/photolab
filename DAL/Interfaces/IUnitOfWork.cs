using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IHistoryRepository HistoryRepository { get; }
        
        IQuestionRepository QuestionRepository { get; }
        ITestRepository TestRepository { get; }
        IAnswerRepository AnswerRepository { get; }
        ITestTypeRepository TestTypeRepository { get; }

        Task<int> SaveAsync();
    }
}