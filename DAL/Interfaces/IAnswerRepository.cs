﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Interfaces
{
    public interface IAnswerRepository : IRepository<Answer>
    {
        Answer GetById(int id);
    }
}
