﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITestTypeRepository : IRepository<TestType>
    {
        TestType GetById(int id);
    }
}
