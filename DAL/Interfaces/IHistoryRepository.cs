﻿using System.Linq;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Interfaces
{
    public interface IHistoryRepository : IRepository<History>
    {
        History GetById(int id);
    }
}