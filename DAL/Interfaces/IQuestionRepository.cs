﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IQuestionRepository : IRepository<Question>
    {
        Question GetById(int id);
    }
}
