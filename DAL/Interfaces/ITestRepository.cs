﻿using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Interfaces
{
    public interface ITestRepository : IRepository<Test>
    {
        Test GetById(int id);
    }
}
