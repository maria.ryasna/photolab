using DAL.Interfaces;
using DAL.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly QuizDbContext db;
        private IHistoryRepository _HistoryRepository { get; }

        private IQuestionRepository _QuestionRepository { get; }

        private ITestRepository _TestRepository { get; }
        private ITestTypeRepository _TestTypeRepository { get; }
        private IAnswerRepository _AnswerRepository { get; }

        public UnitOfWork(QuizDbContext db)
        {
            this.db = db;
            _HistoryRepository = new HistoryRepository(db);
            _QuestionRepository = new QuestionRepository(db);
            _TestRepository = new TestRepository(db);
            _TestTypeRepository = new TestTypeRepository(db);
            _AnswerRepository = new AnswerRepository(db);
        }
        public IHistoryRepository HistoryRepository { get { return _HistoryRepository; } }
        public IQuestionRepository QuestionRepository { get { return _QuestionRepository; } }
        public ITestRepository TestRepository { get { return _TestRepository; } }
        public ITestTypeRepository TestTypeRepository { get { return _TestTypeRepository; } }
        public IAnswerRepository AnswerRepository { get { return _AnswerRepository; } }
        public async Task<int> SaveAsync()
        {
            return await db.SaveChangesAsync();
        }
    }
}