﻿using DAL.Entities;
using DAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class AnswerRepository: IAnswerRepository
    {
        private readonly QuizDbContext db;
        public AnswerRepository(QuizDbContext context)
        {
            this.db = context;
        }

        public Answer GetById(int id)
        {
            return db.Answers.Find(id);
        }

        public IQueryable<Answer> GetAll()
        {
            return db.Answers;
        }

        public async Task<Answer> GetByIdAsync(int id)
        {
            return await db.Answers.FindAsync(id);
        }

        public async Task AddAsync(Answer entity)
        {
            await db.Answers.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public void Update(Answer entity)
        {
            db.Answers.Update(entity);
        }

        public void Delete(Answer entity)
        {
            db.Answers.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            db.Answers.Remove(db.Answers.Find(id));
            await db.SaveChangesAsync();
        }

        public void Add(Answer entity)
        {
            db.Answers.AddAsync(entity);
            db.SaveChanges();
        }
    }
}
