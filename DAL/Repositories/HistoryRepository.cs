﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class HistoryRepository: IHistoryRepository
    {
        private readonly QuizDbContext db;
        public HistoryRepository(QuizDbContext context)
        {
            this.db = context;
        }
        public History GetById(int id)
        {
            return db.Histories.Find(id);
        }

        public IQueryable<History> GetAll()
        {
            var result =
                db.Histories
                .Include(test => test.Test);
            return result;
        }

        public async Task<History> GetByIdAsync(int id)
        {
            return await db.Histories.FindAsync(id);
        }

        public async Task AddAsync(History entity)
        {
            entity.Test=db.Tests.Find(entity.TestId);
            await db.Histories.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public void Update(History entity)
        {
            db.Histories.Update(entity);
            db.SaveChanges();
        }

        public void Delete(History entity)
        {
            db.Histories.Remove(entity);
            db.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            db.Histories.Remove(db.Histories.Find(id));
            await db.SaveChangesAsync();
        }

        public void Add(History entity)
        {
            db.Histories.AddAsync(entity);
            db.SaveChanges();
        }

    }
}
