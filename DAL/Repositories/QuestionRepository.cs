﻿using DAL.Entities;
using DAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class QuestionRepository: IQuestionRepository
    {
        private readonly QuizDbContext db;
        public QuestionRepository(QuizDbContext context)
        {
            this.db = context;
        }
        public Question GetById(int id)
        {
            return db.Questions.Find(id);
        }

        public IQueryable<Question> GetAll()
        {
            return db.Questions;
        }

        public async Task<Question> GetByIdAsync(int id)
        {
            return await db.Questions.FindAsync(id);
        }

        public async Task AddAsync(Question entity)
        {
            await db.Questions.AddAsync(entity);
        }

        public void Update(Question entity)
        {
            db.Questions.Update(entity);
        }

        public void Delete(Question entity)
        {
            db.Questions.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            db.Questions.Remove(db.Questions.Find(id));
            await db.SaveChangesAsync();
        }
        public void Add(Question entity)
        {
            db.Questions.AddAsync(entity);
            db.SaveChanges();
        }
    }
}
