﻿using DAL.Entities;
using DAL.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TestTypeRepository: ITestTypeRepository
    {
        private readonly QuizDbContext db;
        public TestTypeRepository(QuizDbContext context)
        {
            this.db = context;
        }
        public TestType GetById(int id)
        {
            return db.TestTypes.Find(id);
        }

        public IQueryable<TestType> GetAll()
        {
            return db.TestTypes;
        }

        public async Task<TestType> GetByIdAsync(int id)
        {
            return await db.TestTypes.FindAsync(id);
        }

        public async Task AddAsync(TestType entity)
        {
            await db.TestTypes.AddAsync(entity);
            await db.SaveChangesAsync();
        }

        public void Update(TestType entity)
        {
            db.TestTypes.Update(entity);
        }

        public void Delete(TestType entity)
        {
            db.TestTypes.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            db.TestTypes.Remove(db.TestTypes.Find(id));
            await db.SaveChangesAsync();
        }

        public void Add(TestType entity)
        {
            db.TestTypes.AddAsync(entity);
            db.SaveChanges();
        }
    }
}
