﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TestRepository: ITestRepository
    {
        private readonly QuizDbContext db;
        public TestRepository(QuizDbContext context)
        {
            this.db = context;
        }
        public Test GetById(int id)
        {
            return db.Tests.Find(id);
        }

        public IQueryable<Test> GetAll()
        {
            var result =
                db.Tests
                .Include(test => test.Tests)
                .Include(q => q.Questions);
            return result;
        }

        public async Task<Test> GetByIdAsync(int id)
        {
            var result = await db.Tests
                .Include(test => test.Questions).ThenInclude(test => test.Answers)
                .Include(test => test.Tests)
                .Include(test => test.TestType)
                .FirstOrDefaultAsync(i => i.Id == id);
            return result;
        }

        public async Task AddAsync(Test entity)
        {
            await db.Tests.AddAsync(entity);
        }

        public void Update(Test entity)
        {
            db.Tests.Update(entity);
        }

        public void Delete(Test entity)
        {
            db.Tests.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            db.Tests.Remove(db.Tests.Find(id));
            await db.SaveChangesAsync();
        }

        public void Add(Test entity)
        {
            db.Tests.AddAsync(entity);
            db.SaveChanges();
        }
    }
}
