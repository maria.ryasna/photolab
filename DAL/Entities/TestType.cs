﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class TestType: BaseEntity
    {
            public string Name { get; set; }
            public virtual ICollection<Test> TestTypes { get; set; }
    }
}
