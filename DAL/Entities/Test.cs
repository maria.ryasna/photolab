﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace DAL.Entities
{
    public class Test : BaseEntity
    {
        public string Name { get; set; }
        public int TestTypeId { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<History> Tests { get; set; }
        public virtual TestType TestType { get; set; }
    }
}
