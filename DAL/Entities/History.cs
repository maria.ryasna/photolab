﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class History : BaseEntity
    {
        public string UserId { get; set; }
        public int TestId { get; set; }
        public DateTime DatePass { get; set; }
        public int Score { get; set; }
        public virtual Test Test { get; set; }
    }
}
