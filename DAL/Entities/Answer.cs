﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Answer : BaseEntity
    {
        public int QuestionId { get; set; }
        public string Name { get; set; }
        public bool IsRightAnswer { get; set; }
        public virtual Question Test { get; set; }
    }
}
