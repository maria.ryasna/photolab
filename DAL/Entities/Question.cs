﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Question : BaseEntity
    {
        public int TestId { get; set; }
        public string Statement { get; set; }
        public virtual Test Test { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
    }
}
