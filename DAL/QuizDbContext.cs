
using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class QuizDbContext : DbContext
    {
        public QuizDbContext(DbContextOptions<QuizDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<History> Histories { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestType> TestTypes { get; set; }
        public DbSet<Answer> Answers { get; set; }
    }
}