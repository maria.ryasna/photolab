﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Identity.DTO
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
        public int Age { get; set; }
    }
}
