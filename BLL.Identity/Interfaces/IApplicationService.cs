﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace BLL.Identity.Interfaces
{
    public interface IApplicationService
    {
        Task Register(string email, string name, string password);
        Task<IdentityUser> Login(string email, string password, bool rememberMe);
        Task Logout();
        Task<string> GetCurrentRole(IdentityUser user);

        Task<string> GetUserId(string username);

        Task<IdentityUser> GetUserById(string id);
    }
}
