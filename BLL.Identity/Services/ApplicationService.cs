﻿using BLL.Identity.Interfaces;
using BLL.Identity.Validation;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Identity.Services
{
    public class ApplicationService: IApplicationService
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public ApplicationService(UserManager<IdentityUser> _userManager, SignInManager<IdentityUser> _signInManager, RoleManager<IdentityRole> _roleManager)
        {
            userManager = _userManager;
            signInManager = _signInManager;
            roleManager = _roleManager;
        }

        public async Task Register(string email, string username, string password)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user!=null)
            {
                throw new UserException("This user with email already exists!");
            }

            var userName= await userManager.FindByNameAsync(username);
            if (userName != null)
            {
                throw new UserException("This user with name already exists!");
            }

            var newUser = new IdentityUser
            {
                UserName = username,
                Email = email
            };

            var userCreationResult = await userManager.CreateAsync(newUser, password);
            if (!userCreationResult.Succeeded)
            {
                throw new UserException("Invalid user information!");
            }

            await userManager.AddToRoleAsync(newUser, "User");

            var emailConfirmationToken = await userManager.GenerateEmailConfirmationTokenAsync(newUser);

        }

        public async Task<string> GetCurrentRole(IdentityUser user)
        {
            var role = await userManager.GetRolesAsync(user);
            return role.FirstOrDefault();
        }

        public async Task<IdentityUser> Login(string email, string password, bool rememberMe)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user==null)
            {
                throw new UserException("Invalid email!");
            }
            user.EmailConfirmed = true; 
            var passwordSignInResult = await signInManager.PasswordSignInAsync(user, password, isPersistent: rememberMe, lockoutOnFailure: false);
            if (!passwordSignInResult.Succeeded)
            {
                throw new UserException("Invalid password!");
            }
            return user;
        }
         public async Task Logout()
        {
            await signInManager.SignOutAsync();
        }

        public async Task<string> GetUserId(string username)
        {
            IdentityUser user = await userManager.FindByNameAsync(username);
            return await userManager.GetUserIdAsync(user);
        }

        public async Task<IdentityUser> GetUserById(string id)
        {
            return await userManager.FindByIdAsync(id);
        }
    }
}
