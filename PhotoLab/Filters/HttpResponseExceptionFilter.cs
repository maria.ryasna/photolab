﻿using BLL.Identity.Validation;
using BLL.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PhotoLab.Filters
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order { get; } = int.MaxValue - 10;

        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context) 
        {
            if (context.Exception is UserException exception)
            {
                context.Result = new ObjectResult(exception.Message)
                {
                    StatusCode = 404,
                };
                context.ExceptionHandled = true;
            }
            else
            if (context.Exception is LabException TestException)
            {
                context.Result = new ObjectResult(TestException.Message)
                {
                    StatusCode = 404,
                };
                context.ExceptionHandled = true;
            }
            else
            if (context.Exception is Exception ServerException)
                {
                    context.Result = new ObjectResult(ServerException.Message)
                    {
                        StatusCode = 500,
                    };
                    context.ExceptionHandled = true;
                }
        }
    }
}
