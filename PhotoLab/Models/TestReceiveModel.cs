﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class TestReceiveModel
    {
        public string UserName { get; set; }
        public TestToPassModel Test {get;set;}
    }
}
