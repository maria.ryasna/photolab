﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class TestTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
