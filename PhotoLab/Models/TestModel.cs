﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TestTypeId { get; set; }
        public ICollection<int> QuestionsIds { get; set; }
        public ICollection<string> UsersIds { get; set; }
    }
}
