﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class QuestionToPassModel
    {
        public int Id { get; set; }
        public int TestId { get; set; }
        public string Statement { get; set; }
        public ICollection<AnswerModel> Answers { get; set; }
    }
}
