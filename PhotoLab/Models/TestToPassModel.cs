﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class TestToPassModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public TestTypeModel TestType { get; set; }
        public ICollection<QuestionToPassModel> Questions { get; set; }
        public ICollection<string> UsersIds { get; set; }
    }
}
