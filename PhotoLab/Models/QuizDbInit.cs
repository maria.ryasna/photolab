﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Models
{
    public class QuizDbInit
    {
        public static async Task InitializeAsync(IUnitOfWork uow)
        {
            if (!uow.TestTypeRepository.GetAll().Any())
            {
                uow.TestTypeRepository.Add(new DAL.Entities.TestType {Name = "Beginner" });
                uow.TestTypeRepository.Add(new DAL.Entities.TestType {Name = "Middle" });
                uow.TestTypeRepository.Add(new DAL.Entities.TestType {Name = "Advanced" });
            }

            await uow.SaveAsync();
            if (!uow.TestRepository.GetAll().Any())
            {
                uow.TestRepository.Add(new DAL.Entities.Test { Name= "Introduction to Photography", TestTypeId=1});
                uow.TestRepository.Add(new DAL.Entities.Test { Name = "Photography Terms", TestTypeId=2 });
                uow.TestRepository.Add(new DAL.Entities.Test { Name = "Practical Application", TestTypeId=3 });
            }

            await uow.SaveAsync();
            if (!uow.QuestionRepository.GetAll().Any())
            {
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement= "Choose three main settings", TestId=1 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "What is exposure?", TestId = 1 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Which ISO value would produce a photo with the most image noise or grain?", TestId = 1 });

                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Hot shoe is...", TestId = 2 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Softbox is...", TestId = 2 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Golden ratio is...", TestId = 2 });

                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Choose the best lens for landscape photography", TestId = 3 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Which aperture use for landscape photography?", TestId = 3 });
                uow.QuestionRepository.Add
                    (new DAL.Entities.Question { Statement = "Choose the best lens for portrait photography.", TestId = 3 });
            }

            await uow.SaveAsync();

            
            if (!uow.AnswerRepository.GetAll().Any())
            {
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name="ISO", QuestionId=1, IsRightAnswer=true});
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Bokeh", QuestionId = 1, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Aperture", QuestionId = 1, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Shutter speed", QuestionId = 1, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Exposure", QuestionId = 1, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Hot shoe", QuestionId = 1, IsRightAnswer = false });

                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Blur", QuestionId = 2, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name =  "DOF", QuestionId = 2, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Exposure time", QuestionId = 2, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Focus", QuestionId = 2, IsRightAnswer = false });

                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Low", QuestionId = 3, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "High", QuestionId = 3, IsRightAnswer = true });


                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "is one of the most popular subjects in photography", QuestionId = 4, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "a mounting point on the top of a camera to attach a flash unit and other compatible accessories", QuestionId = 4, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "the blurred quality or effect seen in the out-of-focus portion of a photograph taken with a narrow depth of field", QuestionId = 4, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "a hole or an opening through which light travels", QuestionId = 4, IsRightAnswer = false });

                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "a device used to measure the amount of light", QuestionId = 5, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "is what the photographer looks through to compose, and, in many cases, to focus the picture", QuestionId = 5, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "a displacement or difference in the apparent position of an object viewed along two different lines of sight, and is measured by the angle or semi-angle of inclination between those two lines", QuestionId = 5, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "an enclosure designed to fit around an artificial light source, such as a flash tube or halogen lamp", QuestionId = 5, IsRightAnswer = true });

                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "rule of thirds", QuestionId = 6, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "rule of four", QuestionId = 6, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "rule of michelangelo buonarroti", QuestionId = 6, IsRightAnswer = false });




                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "10-18mm", QuestionId = 7, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "50mm", QuestionId = 7, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "100mm", QuestionId = 7, IsRightAnswer = false });



                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "f/2.8", QuestionId = 8, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "f/1.4", QuestionId = 8, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "f/5.6", QuestionId = 8, IsRightAnswer = true });



                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Сanon EF 85мм f/ 1.2L II USM", QuestionId = 9, IsRightAnswer = false });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Canon EF 17-40mm f/ 4L", QuestionId = 9, IsRightAnswer = true });
                uow.AnswerRepository.Add
                    (new DAL.Entities.Answer { Name = "Nikon AF-S FX NIKKOR 18-35 мм f / 3,5-4,5G", QuestionId = 9, IsRightAnswer = true });

            }
            await uow.SaveAsync();
        }
    }
}
