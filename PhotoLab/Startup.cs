using AuthOpt;
using AutoMapper;
using BLL;
using BLL.Identity;
using BLL.Identity.Interfaces;
using BLL.Identity.Services;
using BLL.Interfaces;
using BLL.Services;
using DAL;
using DAL.Interfaces;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using PhotoLab.Filters;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace PhotoLab
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(
                options=>
                options.Filters.Add(new HttpResponseExceptionFilter()));

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutomapperProfile());
                mc.AddProfile(new AutomapperPLProfile());
            });

            IdentityModelEventSource.ShowPII = true;

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("SqlConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            { 
                options.SignIn.RequireConfirmedAccount = true;
                options.Password.RequiredLength = 5;   // ����������� �����
                options.Password.RequireNonAlphanumeric = false;   // ��������� �� �� ���������-�������� �������
                options.Password.RequireLowercase = false; // ��������� �� ������� � ������ ��������
                options.Password.RequireUppercase = false; // ��������� �� ������� � ������� ��������
                options.Password.RequireDigit = false;
            })
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            services.AddDbContext<DbContext, QuizDbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<ITestService, TestService>();
            services.AddScoped<IHistoryService, HistoryService>();
            services.AddScoped<IApplicationService, ApplicationService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<QuizDbContext>();

            

            services.AddAuthentication(
                op=>
                 {
                     op.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                     op.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                 })
                    .AddJwtBearer(options =>
                    {
                       
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = AuthOptions.ISSUER,
                            ValidateAudience = true,
                            ValidAudience = AuthOptions.AUDIENCE,
                            ValidateLifetime = true,
                            IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                            ValidateIssuerSigningKey = true,
                        };
                    });


            var authOptionsCongiguration = Configuration.GetSection("Auth");
            services.Configure<AuthOptions>(authOptionsCongiguration);


            services.AddControllersWithViews();

            //CrossDom request
            services.AddCors(options =>
            options.AddDefaultPolicy(
                builder=>
                {
                    builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
                })
            );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //ang params
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(builder => builder.WithOrigins("http://localhost:4200")
            //DELETE
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                           //DELETE         
                                    );


            app.UseAuthentication();
            app.UseRouting();//must be bellow UseAuth for Core 3.0 and upper
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

        }
    }
}
