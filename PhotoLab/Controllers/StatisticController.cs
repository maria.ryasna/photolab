﻿using AutoMapper;
using BLL.DTO;
using BLL.Identity.Interfaces;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticController : ControllerBase
    {
        private readonly IHistoryService historyService;
        private readonly IApplicationService service;
        IMapper map { get; set; }

        public StatisticController(IHistoryService _historyService, IMapper _map, IApplicationService _service)
        {
            historyService = _historyService;
            map = _map;
            service = _service;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<HistoryModel>> GetAll()
        {
            var result = historyService.GetAll();
            return Ok(historyService.GetAll());
        }

        [HttpPost, Route("receivetest")]
        [Authorize]
        public async Task<ActionResult> GetReceiveTest([FromBody] TestReceiveModel TestReceive)
        {
                var userid = await service.GetUserId(TestReceive.UserName);
                await historyService.MakeHistoryTestPass(map.Map<TestToPassDTO>(TestReceive.Test), userid);
                return Ok();
        }


        [HttpDelete, Route("delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DeleteHistoryById(int id)
        {
                await historyService.DeleteByIdAsync(id);
                return Ok("Excellent!");
        }

    }
}
