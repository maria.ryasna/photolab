﻿using AuthOpt;
using BLL.Identity.Interfaces;
using BLL.Identity.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;

namespace PhotoLab.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IApplicationService service;

        public AccountController(IApplicationService _service)
        {
            service = _service;

        }
        [HttpPost, Route("Register")]
        public async Task<IActionResult> Register([FromBody] UserModel newUser)
        {
            await service.Register(newUser.Email, newUser.Username, newUser.Password);
            return Ok();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] UserModel loginUser)
        {
                IdentityUser user=await service.Login(loginUser.Email, loginUser.Password, loginUser.RememberMe);
                var role = await service.GetCurrentRole(user);
                var token = Token(loginUser.Email, loginUser.Password, role);
                return Ok(token);
        }

        [HttpGet, Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            await service.Logout();
            return Ok("You success logout!");
        }

        [HttpGet("{id}")]
        [Authorize(Roles ="Admin")]
        public async Task<ActionResult<UserModel>> GetUserById(string id)
        {
            var user = await service.GetUserById(id);
            var newUserModel = new UserModel
            {
                Username = user.UserName,
                Email=user.Email
            };
            return Ok(newUserModel);
        }

        [HttpGet("token")]
        public IActionResult Token(string username, string password, string role)
        {
            var identity = GetIdentity(username, password);
            if (identity == null)
            {
                return BadRequest("Invalid username or password.");
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Result.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            var response = new
            {
                access_token = encodedJwt,
                username = identity.Result.Name,
                role=role
            };
            return Ok(response);
        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            IdentityUser person = await service.Login(username, password, true);
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.UserName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, await service.GetCurrentRole(person))
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            // if user not found
            return null;
        }
    }
}