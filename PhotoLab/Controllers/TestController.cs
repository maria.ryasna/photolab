﻿using AutoMapper;
using BLL.DTO;
using BLL.Identity.Interfaces;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.HttpSys;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoLab.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ITestService testService;
        IMapper map { get; set; }

        public TestController(ITestService _testService, IMapper _map)
        {
            testService = _testService;
            map = _map;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TestModel>> GetAll()
        {
            var result =
                from test in testService.GetAll()
                select map.Map<TestModel>(test);
            return Ok(result);
        }


        [HttpGet("pass/{id}")]
        [Authorize]
        public async Task<ActionResult<TestToPassDTO>> GetModelTestToPass(int id)
        {
                var result = await testService.GetModelTestById(id);
                return Ok(result);
        }

    }
}
