﻿using AutoMapper;
using BLL.DTO;
using PhotoLab.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PhotoLab
{
    public class AutomapperPLProfile : Profile
    {
        public AutomapperPLProfile()
        {
            CreateMap<HistoryDTO, HistoryModel>().ReverseMap();
            CreateMap<TestDTO, TestModel>().ReverseMap();
            CreateMap<TestTypeDTO, TestTypeModel>().ReverseMap();
            CreateMap<QuestionToPassDTO, QuestionToPassModel>().ReverseMap();
            CreateMap<AnswerDTO, AnswerModel>().ReverseMap();
            CreateMap<TestToPassDTO, TestToPassModel>().ReverseMap();
        }
    }
}
